#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#define MAX 100

int main(void)
{
	char cadena[MAX];
	int fd;
	int create_error,read_error;
	create_error=mkfifo("/home/alberto/tuberia",0777);
	if(create_error==-1){ 
		perror("Error al crear la tubería\n");
		exit(1);
	}
	fd=open("/home/alberto/tuberia",O_RDONLY);
	if(fd==-1){
		perror("Error al abrir la tubería\n");
		exit(1);
	}
	printf("Inicio del Juego \n\n");	

	while(1){
		read_error=read(fd,cadena,sizeof(cadena));
		if(read_error==-1){
			perror("Error al leer la tubería\n");
			exit(1);
		}
		else if(cadena[0]=='F'){
			printf("%s",cadena);
			break;
		}
		else{
			printf("%s\n",cadena);
		}
	}

	close(fd);
	unlink("/home/alberto/tuberia");
}
